<!DOCTYPE html>
<html>
	<head>
		 	<!-- Bootstrap -->
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="Author" content="Paul Beynon">
	    
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">		 -->
		<!-- Site -->
	
		<!-- [if lt IE 9] -->
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<!-- [endif] -->

		<title>Yoko's Kitchen</title>
		<link rel="stylesheet" href="./css/app.css">
		<link rel="shortcut icon" href="./img/favicon.ico">
	</head>

	<body>

		<main>

			<header>
				<h1>Yoko's Kitchen</h1>
				<a href="#nav">Menu</a>
			</header>

			<article>
				<figure>
					<img src="./img/bok-choi.jpg" alt="Bok Choi">
					<figcaption>Bok Choi</figcaption>
				</figure>
				<h1>Japanese Vegetarian</h1>
				<h3>Five Week Course in London</h3>
				<p>A five week introduction to traditional Japanese vegetarian meals, teaching you a selection of rice and noodle dishes.</p>
			</article>

			<article>
				<figure>
					<img src="./img/teriyaki.jpg" alt="Teriyaki Sauce">
					<figcaption>Teriyaki Sauce</figcaption>
				</figure>
				<h1>Sauces Masterclass</h1>
				<h3>One Day Workshop</h3>
				<p>An intensive one day course looking at how to create the most delicious sauces for use in a range of Japanese cookery.</p>
			</article>

			<aside>

				<section>
					<h2>Popular Recipes</h2>
					<a href="#">Yakitori <span>(grilled chicken)</span></a>
					<a href="">Tsukune <span>(minced chicken patties)</span></a>
					<a href="">Okonomiyaki <span>(savory pancakes)</span></a>
					<a href="">Mitzutaki <span>(chicken stew)</span></a>
				</section>

				<section>
					<h2>Contact</h2>
					<p>Yoko's Kitchen <br> 27 Redchurch Street <br> Shoreditch <br> London E2 7DP</p>
				</section>

			</aside>

			<nav id="nav">
				<ul>
					<li><a href="">Home</a></li>
					<li><a href="">Classes</a></li>
					<li><a href="">Catering</a></li>
					<li><a href="">About</a></li>
					<li><a href="">Contact</a></li>
					<li><a href="#">Top</a></li>
				</ul>
			</nav>

			<footer>&copy; 2011 Yoko's Kitchen</footer>

		</main>
	</body>
</html>